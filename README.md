[![pipeline status](https://gitlab.com/estudosdevops/ansible/roles/common/badges/master/pipeline.svg)](https://gitlab.com/estudosdevops/ansible/roles/common/commits/master)

### Role common

- Role generica para instalação de pacotes basicos e configuração de arquivos nos sistemas operações Linux abaixo.

### Sistemas operacionais suportados

- Fedora Workstation
- CentOS7
- Ubuntu

### Todo

- Tasks CentOS7
- Tasks Ubuntu

### Variáveis

- Todas Variáveis estão em ***group_vars*** para cada sistema operacional.

### Pre requesitos

- Install git e ansible na maquina local
- Clona projeto no diretorio de sua preferencia

### Executando playbook local

```yml
ansible-playbook -i hosts.ini -l localhost lint-it.yml
```
